var shoppingFestival = angular.module('shoppingFestival', ['ngRoute', 'ngCookies', 'sfControllers']);

shoppingFestival.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: 'partials/index.html',
        controller: 'IndexController'
      })
      .when('/projects', {
        templateUrl: 'partials/projects.html',
        controller: 'ProjejctsController'
      })
      .when('/cities', {
        templateUrl: 'partials/cities.html'
      })
      .when('/giftBox', {
        templateUrl: 'partials/giftBox.html'
      })
      .when('/user', {
        templateUrl: 'partials/user.html',
        controller: 'UCenterController',
      })
      .when('/signIn', {
        templateUrl: 'partials/signIn.html',
        controller: 'SignInController',
      })
      .when('/taxiRecords', {
        templateUrl: 'partials/taxiRecords.html',
        controller: 'TaxiHistoryController'
      })
      .when('/visitRecords', {
        templateUrl: 'partials/visitRecords.html',
        controller: 'VisitHistoryController'
      })
      .when('/coupon', {
        templateUrl: 'partials/coupon.html',
        controller: 'CouponController'
      })
      .when('/orderSuccess', {
        templateUrl: 'partials/orderSuccess.html'
      })
      .when('/shake', {
        templateUrl: 'partials/shake.html'
      })
      .when('/tos/', {
        templateUrl: 'partials/tos.html'
      })
      .when('/error/', {
        templateUrl: 'partials/error.html'
      })
      .otherwise({
        redirectTo: '/'
      });
  }
]);