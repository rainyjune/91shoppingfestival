var sfControllers = angular.module('sfControllers', []);

sfControllers.controller('IndexController', ['$scope', '$http', '$location',
  function ($scope, $http, $location) {
    var searchObject = $location.search();
    if (!searchObject.project_id || !searchObject.purchase_id) {
      $location.url('error').replace();
      return false;
    }
    
    $scope.showRulePopup = false;
    
    $http.get('./data/index.json').then( function(response) {
      $scope.activity = response.data;
    });
    
    $scope.updatePath = function(path) {
      $location.path(path);
    };
    
    $scope.showRules = function() {
      $scope.showRulePopup = true;
    };
    $scope.hideRules = function() {
      $scope.showRulePopup = false;
    };
  }]);

sfControllers.controller('ProjejctsController', ['$scope', '$http', '$location',
  function($scope, $http, $location) {
    var searchObject = $location.search();
    if (!searchObject.project_id || !searchObject.purchase_id) {
      $location.url('error').replace();
      return false;
    }
    
    $scope.formData = {
      project_id: searchObject.project_id,
      purchase_id: searchObject.purchase_id,
      page: 1,
      q: ""
    };
    $scope.projectList = [];
    $scope.showMore = false;
    $scope.noResultFound = false;
    $scope.loading = false;
    $scope.keyword = "";
    
    loadProjects();
    
    function loadProjects() {
      var url="/activity/purchase/house_list_data.html";
      $scope.loading = true;
      $scope.showMore = false;
      $http.get(url, {params: angular.copy($scope.formData)}).then(function(response){
        $scope.loading = false;
        if ($scope.formData.page === 1) {
          $scope.projectList = response.data.data;
        } else {
          $scope.projectList = $scope.projectList.concat(response.data.data);
        }
        $scope.noResultFound = $scope.projectList.length === 0;
        $scope.showMore = !!response.data.has_next_page;
      });
    }
    
    $scope.loadMore = function() {
      $scope.formData.page++;
      loadProjects();
    };
    
    $scope.searchProject = function() {
      if (!$scope.keyword) { return ;}
      $scope.formData.q = $scope.keyword;
      $scope.formData.page = 1;
      loadProjects();
    };
    
    $scope.updatePath = function(path) {
      $location.path(path);
    };
  }
])
  

sfControllers.controller('SignInController', ['$scope', '$http', '$cookies', '$location', '$timeout',
  function($scope, $http, $cookies, $location, $timeout) {
    var searchObject = $location.search();
    var project_id = searchObject.project_id,
        purchase_id = searchObject.purchase_id;
    if (!project_id || !purchase_id) {
      $location.url('error').replace();
      return false;
    }
    
    var leju_91_purchase_open_id = $cookies.get('leju_91_purchase_open_id');
    if (leju_91_purchase_open_id) {
      $location.path('user').replace();
      return false;
    }
    
    $scope.submitForm = function(form) {
      $scope.scenario = "signIn";
      if (!form.$valid) {
        $scope.showTips = true;
        $timeout(function(){
          $scope.showTips = false;
        }, 2000);
      } else {
        var url = '/activity/purchase/user_login.html?project_id={{project_id}}&purchase_id={{purchase_id}}';
        $http({
          method: 'POST',
          url: url.replace('{{project_id}}', project_id).replace('{{purchase_id}}', purchase_id),
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          },
          transformRequest: function (obj) {
              var str = [];
              for (var p in obj)
                  str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
              return str.join("&");
          },
          data: {project_id: parseInt(project_id), purchase_id: parseInt(purchase_id), telphone: parseInt($scope.telphone), code: parseInt($scope.code)}
        }).then(function(response){
            if (response.data.success === true) {
              $location.path('user').replace();
            } else {
              $scope.showTips = true;
              $scope.scenario = "ajaxInfo";
              $scope.ajaxMsg = response.data.msg;
              $timeout(function(){
                $scope.showTips = false;
              }, 2000);
            }
          });
      }
    };
    
    $scope.sendSMS = function(form) {
      $scope.scenario = "sendSMS";
      if (form.telphone.$error.required || form.telphone.$error.pattern) {
        $scope.showTips = true;
        $timeout(function(){
          $scope.showTips = false;
        }, 2000);
        return;
      }
      var url = '/activity/purchase/send_sms_code.html';
      $http.get(url, {params: {project_id: project_id, purchase_id: purchase_id, telphone: $scope.telphone}}).
        then(function(response){
          $scope.showTips = true;
          $scope.scenario = "ajaxInfo";
          $scope.ajaxMsg = response.data.message;
          $timeout(function(){
            $scope.showTips = false;
          }, 2000);
        });
    };
    
  }]);
  
sfControllers.controller('UCenterController', ['$scope', '$http', '$cookies', '$location',
  function($scope, $http, $cookies, $location) {
    var searchObject = $location.search();
    if (!searchObject.project_id || !searchObject.purchase_id) {
      $location.url('error').replace();
      return false;
    }
    var leju_91_purchase_open_id = $cookies.get('leju_91_purchase_open_id');
    if (!leju_91_purchase_open_id) {
      $location.path('signIn').replace();
      return false;
    }
    
    var url = "data/user.json";
    $http.get(url).then(function(response) {
      $scope.userData = response.data;
    });
    
    $scope.orderTaxi = function() {
      if ($scope.userData.residue_count > 0) {
        $scope.updatePath('projects');
      }
    };
    
    $scope.updatePath = function(path) {
      $location.path(path);
    };
    
  }]);

sfControllers.controller('TaxiHistoryController', ['$scope', '$http', '$cookies', '$location',
  function($scope, $http, $cookies, $location) {
    var searchObject = $location.search();
    if (!searchObject.project_id || !searchObject.purchase_id) {
      $location.url('error').replace();
      return false;
    }
    var leju_91_purchase_open_id = $cookies.get('leju_91_purchase_open_id');
    if (!leju_91_purchase_open_id) {
      $location.path('signIn').replace();
      return false;
    }
    var url = "data/taxiRecords.json";
    $http.get(url).then(function(response) {
      $scope.records = response.data;
    });
  }]);

sfControllers.controller('VisitHistoryController', ['$scope', '$http', '$cookies', '$location',
  function($scope, $http, $cookies, $location) {
    var searchObject = $location.search();
    if (!searchObject.project_id || !searchObject.purchase_id) {
      $location.url('error').replace();
      return false;
    }
    var leju_91_purchase_open_id = $cookies.get('leju_91_purchase_open_id');
    if (!leju_91_purchase_open_id) {
      $location.path('signIn').replace();
      return false;
    }
    var url = "data/visitRecords.json";
    $http.get(url).then(function(response) {
      $scope.records = response.data;
    });
  }]);

sfControllers.controller('CouponController', ['$scope', '$http', '$cookies', '$location',
  function($scope, $http, $cookies, $location) {
    var searchObject = $location.search();
    if (!searchObject.project_id || !searchObject.purchase_id) {
      $location.url('error').replace();
      return false;
    }
    var leju_91_purchase_open_id = $cookies.get('leju_91_purchase_open_id');
    if (!leju_91_purchase_open_id) {
      $location.path('signIn').replace();
      return false;
    }
    var url = "data/rules.json";
    $http.get(url).then(function(response) {
      $scope.rules = response.data;
    });
    
    var couponListUrl = "data/couponList.json";
    $http.get(couponListUrl).then(function(response) {
      $scope.couponList = response.data;
    });
  }]);
